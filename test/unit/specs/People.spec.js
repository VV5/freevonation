import { shallowMount } from '@vue/test-utils'
import People from '@/components/People'

describe('People', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(People)
  })

  test('', () => {
    expect(wrapper.isVueInstance()).toBe(true)
  })
})
