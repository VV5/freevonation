import Vue from 'vue'
import Router from 'vue-router'
import Freevonation from '@/components/Freevonation'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'FreeVonation',
      component: Freevonation,
      meta: {
        title: 'FreeVonation'
      }
    }
  ]
})
