import Vue from 'vue'
import App from '@/App'
import store from '@/store'
import router from '@/router'
import bodymovin from 'bodymovin'
import VueOdometer from 'vue-odometer'
import jQuery from 'jquery'

window.$ = window.jQuery = jQuery

require('odometer/themes/odometer-theme-default.css')

window.bodymovin = bodymovin

Vue.config.productionTip = false

Vue.component('vue-odometer', VueOdometer)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App),
  components: {App},
  template: '<App/>'
})
